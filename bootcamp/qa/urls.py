from django.conf.urls import url
from django.views.generic import TemplateView
from bootcamp.qa import views

app_name = 'qa'
urlpatterns = [
    url(r'^$', views.QuestionListView.as_view(), name='index_noans'),
    url(r'^answered/$', views.QuestionAnsListView.as_view(), name='index_ans'),
    url(r'^indexed/$', views.QuestionsIndexListView.as_view(), name='index_all'),
    url(r'^question-detail/(?P<pk>\d+)/$', views.QuestionDetailView.as_view(), name='question_detail'),
    url(r'^edit-question/(?P<pk>\d+)/$', views.EditQuestion.as_view(), name='edit_question'),
    url(r'^ask-question/$', views.CreateQuestionView.as_view(), name='ask_question'),
    url(r'^propose-answer/(?P<question_id>\d+)/$', views.CreateAnswerView.as_view(), name='propose_answer'),
    url(r'^edit-answer/(?P<pk>[-\w]+)/$', views.EditAnswer.as_view(), name='edit_answer'),
    url(r'^delete-answer/(?P<pk>[-\w]+)/$', views.DeleteAnswer.as_view(), name='delete_answer'),
    url(r'^question/vote/$', views.question_vote, name='question_vote'),
    url(r'^answer/vote/$', views.answer_vote, name='answer_vote'),
    url(r'^accept-answer/$', views.accept_answer, name='accept_answer'),
    url(r'^download/$', TemplateView.as_view(template_name="qa/download_question.html"), name='download'),
    url(r'^downloads/questions/(?P<pk>\d+)/$', views.download_question, name='download_question'),
    url(r'^downloads/questions/$', views.post_file_downloaded_status, name='task_status'),
    url(r'^downloads/questions/(?P<pk>\d+)/file/$', views.get_downloaded_question_file, name='get_file')
]
