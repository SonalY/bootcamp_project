from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Search


connections.create_connection(host='e67a578a73ea4251a27d1cd437c6ab97.us-east-1.aws.found.io',
                              http_auth='elastic:BtXMJLtU2r6MXOqtuTgNPlxg')


class QuestionIndex(DocType):
    title = Text()
    content = Text()

    class Index:
        name = 'question_index'


def search_in_elastic_cloud(search_query):
    s = Search(index='question_index').filter('query_string', query='*'+search_query+'*',
                                              fields=['title', 'content', 'tags'])
    QuestionQuerySet = s.execute()
    response = [query['title'] for query in QuestionQuerySet]
    print("response")
    print(response)
    return response
