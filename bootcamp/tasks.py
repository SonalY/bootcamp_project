from bootcamp.qa.models import Question
from celery import shared_task
import json
import os


@shared_task
def create_file(user_id):
    questions = Question.objects.filter(user=user_id)
    file_name = str(user_id)+'.json'
    file_path = os.path.join(os.environ.get('HOME'), file_name)
    questions_list = []
    for question in questions:
        question_dictionary = {}
        question_dictionary["title"] = question.title
        question_dictionary["content"] = question.content
        questions_list.append(question_dictionary)
    with open(file_path, "w+") as ques_file:
        json.dump(questions_list, ques_file)
